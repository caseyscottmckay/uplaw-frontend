/* eslint-disable react/no-array-index-key,react/no-unused-state */
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Document from '../../../document/Document';

export default () => (
  <Switch>
    <Route path="/documents/:documentId" component={Document} />

  </Switch>
);
