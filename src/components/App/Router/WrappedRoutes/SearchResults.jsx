import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SearchResult from '../../../SearchResults/components/SearchResult';

export default () => (
  <Switch>
    <Route exact path="/search" component={SearchResult} />
  </Switch>
);
