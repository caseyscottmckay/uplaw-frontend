/* eslint-disable react/no-array-index-key,react/no-unused-state */
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Search from '../../../search/index';

export default () => (
  <Switch>
    <Route path="/search" component={Search} />

  </Switch>
);
