/*eslint-disable*/
import React from 'react';
import './Search.css';
import axios from 'axios';
import {Link} from 'react-router-dom';


class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      loading: false,
      message: '',
      documents: '',
    };
    this.cancel = '';
  }


  fetchSearchResults = (updatedPageNo = '', query) => {
    const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
    // By default the limit of results is 20

    if (this.cancel) {
      // Cancel the previous request before making a new request
      this.cancel.cancel();
    }
    // Create a new CancelToken
    this.cancel = axios.CancelToken.source();

    /*axios
      .get(searchUrl, {
        cancelToken: this.cancel.token,
      })
      .then((res) => {
        const resultNotFoundMsg = !res.data.hits.length
          ? 'There are no more search results. Please try a new search.'
          : '';
        this.setState({
          results: res.data.hits,
          message: resultNotFoundMsg,
          loading: false,
        });
      });*/
    axios.get(`http://localhost:8080/api/documents/search/${query}`, {
      cancelToken: this.cancel.token,
    })
      .then(response => {

        const state = Object.assign({}, this.state);
        state.documents = response.data;
        this.setState(state);
      }).catch((error) => {
      if (axios.isCancel(error) || error) {
        this.setState({
          loading: false,
          message: 'Failed to fetch results.Please check network',
        });
      }
    }).catch(error => {
      console.error('Error path:', error.response);
    });
  };
  handleOnInputChange = (event) => {
    const query = event.target.value;
    if (!query) {
      this.setState({query, results: {}, message: ''});
    } else {
      this.setState({query, loading: true, message: ''}, () => {
        this.fetchSearchResults(1, query);
      });
    }
  };

  renderSearchResults = () => {
    return (
      <div className="search-results">
        {
          this.state.documents ? (
            <div className="search-result">
              {this.state.documents.map(document =>
                <div key={document.id}><Link
                  to={`/documents/${document.id}`}>{document.title} - {document.citation}</Link><br/>
                  {document.slug}<br/>
                  {document.date_created}<br/>
                </div>

              )}
            </div>

          ) : null
        }
      </div>

    )
  };

  render() {
    const {query} = this.state;
    return (
      <div className="container">
        {/*Heading*/}
        <h2 className="heading">UpLaw Search</h2>
        {/*Search Input*/}
        <label className="search-label" htmlFor="search-input">
          <input
            type="text"
            value={query}
            id="search-input"
            placeholder="Search..."
            onChange={this.handleOnInputChange}
          />
          <i className="fa fa-search search-icon"/>
        </label>

        {/*Result*/}
        {this.renderSearchResults()}
      </div>
    )
  }
}

export default Search;