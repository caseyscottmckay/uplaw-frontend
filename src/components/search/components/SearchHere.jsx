/* eslint-disable */
import React, { PureComponent } from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import MagnifyIcon from 'mdi-react/MagnifyIcon';
import axios from 'axios';

import SearchResult from './SearchResult';
import results from './results';


export default class SearchHere extends PureComponent {
  constructor() {
    super();
    this.state = {
      rows: results,
      pageOfItems: [],
      query: '',
      loading: false,
      message: '',
      documents: '',
    };
    this.cancel = '';
    this.onChangePage = this.onChangePage.bind(this);
  }

  onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems });
  }

  fetchSearchResults = (updatedPageNo = '', query) => {
    const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
    // By default the limit of results is 20

    if (this.cancel) {
      // Cancel the previous request before making a new request
      this.cancel.cancel();
    }
    // Create a new CancelToken
    this.cancel = axios.CancelToken.source();

    /*axios
      .get(searchUrl, {
        cancelToken: this.cancel.token,
      })
      .then((res) => {
        const resultNotFoundMsg = !res.data.hits.length
          ? 'There are no more search results. Please try a new search.'
          : '';
        this.setState({
          results: res.data.hits,
          message: resultNotFoundMsg,
          loading: false,
        });
      });*/
    axios.get(`http://localhost:8080/api/documents/search/${query}`, {
      cancelToken: this.cancel.token,
    })
      .then(response => {

        const state = Object.assign({}, this.state);
        state.documents = response.data;
        this.setState(state);
      }).catch((error) => {
      if (axios.isCancel(error) || error) {
        this.setState({
          loading: false,
          message: 'Failed to fetch results.Please check network',
        });
      }
    }).catch(error => {
      console.error('Error path:', error.response);
      });
    };
    handleOnInputChange = (event) => {
      const query = event.target.value;
      if (!query) {
       this.setState({ query, results: {}, message: '' });
      } else {
       this.setState({ query, loading: true, message: '' }, () => {
          this.fetchSearchResults(1, query);
      });
    }
  };

  renderSearchResults = () => {
    return (
      <div className="search-results">
        {
          this.state.documents ? (
            <div className="search-result">
              {this.state.documents.map(document =>
                <SearchResult
                  key={document.id}
                  title={document.title}
                  link={`/documents/${document.id}`}
                  preview={document.date_created}
                />
              )}
            </div>

          ) : null
        }
      </div>

    )
  };


  render () {
    const { query } = this.state;
    return (
      <Col md={12} lg={12}>
        <Card>
          <CardBody>
            <div className="card__title">
              <h5 className="bold-text">Search here</h5>
            </div>
            <form className="form">
              <div className="form__form-group">
                <div className="form__form-group-field">
                  <label className="search-label" htmlFor="search-input">
                    <input
                      type="text"
                      value={query}
                      id="search-input"
                      placeholder="Search..."
                      onChange={this.handleOnInputChange}
                    />
                  </label>
                  <div className="form__form-group-icon">
                    <MagnifyIcon/>
                  </div>
                </div>
              </div>
            </form>

            <h4 className="typography-message">{'Search result for '}{query}</h4>
            <h4 className="subhead typography-message">Found 18 results</h4>
            <div>
              {this.renderSearchResults()}
            </div>
          </CardBody>
        </Card>
      </Col>
    );
  }
}
