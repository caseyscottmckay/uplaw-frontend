/*eslint-disable*/
import React from 'react';
import { Col, Row, Container } from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Search from '../../search/Search';

const background = `${process.env.PUBLIC_URL}/img/landing/header_bg.png`;
const img = `${process.env.PUBLIC_URL}/img/landing/macbook.png`;

const Header = ({ onClick }) => (
  <div className="landing__header" style={{ backgroundImage: `url(${background})` }}>
    <Container>
      <Row>
        <Col md={12}>
          <h2 className="landing__header-title">Welcome to
            <b> UpLaw</b>
          </h2>
          <Search />
          <p className="landing__header-subhead">We provide the resources you need to {' '}.
            <Link className="landing__header-subhead-update" to="/documentation/changelog" target="_blank">
              represent your rights.
            </Link>.
          </p>
          <Link className="landing__btn landing__btn--header" to="/documentation/introduction" target="_blank">
            Check out the docs
          </Link>
          <button type="button" className="landing__btn landing__btn--header" onClick={onClick}>
            Search Now!
          </button>
          {/*<img className="landing__header-img" src={img} alt="macbook" />*/}
        </Col>
      </Row>
    </Container>
  </div>
);

Header.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Header;
